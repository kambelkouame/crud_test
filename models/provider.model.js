const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    
    name: {
        type: String,
        required: 'Full name can\'t be empty'
    },
    dateSave: {
        type: Date,
        default: Date.now()
    },
          
    
});
mongoose.model('provider', schema);