const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    
    name: {
        type: String,
        required: 'Full name can\'t be empty'
    },
   
    email: {
        type: String,
        default: '',
    },
    phone: {
        type: String
        
    },
    providers: {
        type: Array,
       
    },
    dateSave: {
        type: Date,
        required: true,
        default: Date.now()
    },
          
    
});
mongoose.model('client', schema);