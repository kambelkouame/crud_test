require('./config/config');
require('./models/dbconfig/db');


const {
    crudRouter
} = require('./router/crud.router');
const express = require('express');
const morgan = require('morgan');

var app = express();


const swaggerUi = require('swagger-ui-express'),
swaggerDocumen = require('./swagger-crud-test.json');


//documentation  swagger route
app.use("/api-crud", swaggerUi.serve, (...args) => swaggerUi.setup(swaggerDocumen)(...args));


app.use(morgan('dev'));
// middleware
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.use('/api', crudRouter);


app.listen(process.env.PORT, () => console.log(`Server started at port : ${process.env.PORT}`));


