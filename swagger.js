const swaggerAutogen = require('swagger-autogen')();


const doc_crud = {
    info: {
        version: '1.0', // by default: '1.0.0'
        title: 'crud', // by default: 'REST API'
        description: '', // by default: ''
    },
    host: 'localhost:3001', // by default: 'localhost:3000'
    basePath: '/api', // by default: '/'
    schemes: [], // by default: ['http']
    consumes: [], // by default: ['application/json']
    produces: [], // by default: ['application/json']
    tags: [ // by default: empty Array
        {
            name: 'crud', // Tag name
            description: 'crud', // Tag description
        },
        // { ... }
    ],
    securityDefinitions: {}, // by default: empty object
    definitions: {}, // by default: empty object
};

const outputFile = './swagger-crud-test.json';
const crudRouter = ['./router/crud.router.js', ];

swaggerAutogen(outputFile, crudRouter, doc_crud).then(() => {
    require('./server.js')
})
