
function respond(res, data, key = 'data') {
    return res.status(200).jsonp({ status: true, [key]: data });
}
module.exports.respond = respond;

function reject(res, message, key = 'message') {
    //return res.status(200).jsonp({ status: false, [key]: message });
    return res.jsonp({ status: false, [key]: message });
}
module.exports.reject = reject;