const express = require('express');
const router = express.Router();

const {updateProvider,deleteByProviderId,getProviderById,getAllProvider,createProvider, updateClient,deleteById,getClientById,getAllClient,insertClient} = require('../controllers/crud.controllers');



router.get("/test", async (req, res, next) => {
    res.jsonp({status:true});
});

//create client
router.post('/create-client', insertClient);
//get all client
router.get('/get-all-client', getAllClient);

//get client by id
router.get('/get-client-id/:id', getClientById);

//delete client
router.get('/get-delete-client/:id', deleteById);
//update client
router.post('/update-client', updateClient);



//create client
router.post('/create-provider', createProvider);
//get all client
router.get('/get-all-provider', getAllProvider);

//get client by id
router.get('/get-provider-id/:id', getProviderById);

//delete client
router.get('/get-delete-provider/:id', deleteByProviderId);
//update client
router.post('/update-provider', updateProvider);
module.exports.crudRouter = router;