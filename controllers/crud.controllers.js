const mongoose = require('mongoose');
const Client = mongoose.model('client');
const Provider = mongoose.model('provider');


const {validator} = require('../config/validator');
const {respond,reject} = require('../config/response');


//insert new client

module.exports.insertClient= async (req, res) => {

    const {name,email,phone,providers}=req.body
    try {
        if (validator.isString(name) == false) return reject(res, "name invalid");
        if (validator.isMail(email) == false) return reject(res, "email invalid");
        if (validator.isPhoneNumber(phone) == false) return reject(res, "phone invalid ");
       
       var client =new Client()
       client.name =name,
       client.email =email,
       client.phone =phone,
       client.providers.push(providers)
       client.save()
       respond(res,client)
    } catch (e) {
       
        return reject(res, e);
    }
}


//get all client
module.exports.getAllClient= async (req, res) => {
    try {
        var client =await  Client.find()
       respond(res,client)
    } catch (e) {
       
        return reject(res, e);
    }
}

//get client by id
module.exports.getClientById= async (req, res) => {
    try {
        var client =await  Client.findOne({_id:req.params.id})
       respond(res,client)
    } catch (e) {
       
        return reject(res, e);
    }
}

//delete client by id
module.exports.deleteById= async (req, res) => {
    try {
            Client.findByIdAndRemove(req.params.id, (err, doc) => {
            if (!err) {
                respond(res,doc)
            }
            else {
                reject(res,err)
            }
        });
      
    } catch (e) {
       
        return reject(res, e);
    }
}
//update client information
module.exports.updateClient= async (req, res) => {
    const {_id,name,email,phone,providers}=req.body
    try {
        var client =await  Client.findOne({_id:_id})
        client.name=name
        client.email =email,
        client.phone =phone,
        client.providers=providers
        client.save()
        respond(res,client)
    } catch (e) {
       
        return reject(res, e);
    }
}


//create provider
module.exports.createProvider= async (req, res) => {
    const {name}=req.body
    try {
        if (validator.isString(name) == false) return reject(res, "name invalid");
        var provider = new Provider()
        provider.name=name
        provider.save()
        respond(res,provider)
    } catch (e) {
       
        return reject(res, e);
    }
}
//get all provider
module.exports.getAllProvider= async (req, res) => {
    try {
        var provider =await  Provider.find()
       respond(res,provider)
    } catch (e) {
       
        return reject(res, e);
    }
}

//get provider by id
module.exports.getProviderById= async (req, res) => {
    try {
        var provider =await  Provider.findOne({_id:req.params.id})
       respond(res,provider)
    } catch (e) {
       
        return reject(res, e);
    }
}

//delete provider by id
module.exports.deleteByProviderId= async (req, res) => {
    try {
        Provider.findByIdAndRemove(req.params.id, (err, doc) => {
            if (!err) {
                respond(res,doc)
            }
            else {
                reject(res,err)
            }
        });
      
    } catch (e) {
       
        return reject(res, e);
    }
}
//update provider information
module.exports.updateProvider= async (req, res) => {
    const {_id,name}=req.body
    try {
        var provider =await  Provider.findOne({_id:_id})
        provider.name=name
        provider.save()
        respond(res,provider)
    } catch (e) {
       
        return reject(res, e);
    }
}
